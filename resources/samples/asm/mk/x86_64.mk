
SRCDIR   = src/x64
SRC      = add.asm implicit_zero_extension.asm

CC	= gcc
CFLAGS	= -O2 -m64 -Wall
LDFLAGS = -O2 -m64
ASM	= fasm
ASMFLAGS = -felf64

O	= o
BIN	= build/$objtype
