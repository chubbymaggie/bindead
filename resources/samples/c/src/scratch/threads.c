
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#include "test.h"


#define NTHREADS 3

void* threadFn ( void *usr)
{
    long id = (long)usr;

    printf ("Thread%u\n", (unsigned int)id);

    return (NULL);
}

void testThreads ( int n)
{

    pthread_t thr[NTHREADS];
    long i;

    puts ("Spawning threads\n");

    for (i = 0; i < NTHREADS; i++) {
        pthread_create (&thr[i], NULL, threadFn, (void*)i);
    }


    puts ("Joining threads\n");

    for (i = 0; i < NTHREADS; i++) {
        pthread_join (thr[i], NULL);
    }
}
