
#include <iostream>
using namespace std;

class X {
  public:
  int x1, x2;
  X(void) { x1=100; x2=101;}
  virtual void foo(void) {
    cout << "X::foo(" << hex << this << ")" << endl;
  };
};

class A: public X {
  public:
  int a1, a2;
  A(void) { a1=1; a2=2;};
  virtual void foo(void) {
    cout << "A::foo(" << hex << this << ")" << endl;
  };
};

class B: public virtual A {
  public:
  int b1, b2;
  B(void) {b1=3; b2=4;};
  virtual void bar(void) {
    cout << "B::bar(" << hex << this << ")" << endl;
  };
};

class C: public virtual A {
  public:
  int c1, c2;
  C(void) {c1=5; c2=6;};
  virtual void foo(void) {
    cout << "C::foo(" << hex << this << ")" << endl;
  };
};

class D: public B, public C {
  public:
  int d1, d2;
  D(void) {d1=7;d2=8;};
  virtual void foo(void) {
    cout << "D::foo(" << hex << this << ")" << endl;
  }
  virtual void bar(void) {
    cout << "D::bar(" << hex << this << ")" << endl;
  }
};

int main (int argc, char* argv[]) {
  D* d = new D();
  d->foo();
  A* a = (A*)d;
  a->foo();
  return 0;
}
