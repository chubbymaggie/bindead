//@summarystart
// Example which illustrates McVeto's ability to deal with
// instruction aliasing.
//@summaryend
//#include "../include/reach.h"

int n;

int foo(int a) { // In the object file, the address of foo is 0
  n += a;
  return 1;
}

int main() {

  int x;
  int y;

  n = y;

  if(y<0)
      n=3;
  else
      n=4;

  asm (
    "mov $2, %eax\n"
    "L1: mov $0xd0ff046a,%edx\n"
    "cmp $1, %eax\n"
    "jz  L2\n"
    "lea foo, %eax\n"
    "lea L1+1, %ebx\n"
    "jmp %ebx\n"
    "L2: xor %edx, %edx\n");

//  if(n == 7)
//    REACHABLE(); // Reachable: 3 + 4 = 7
  
  return (n==7);
}

