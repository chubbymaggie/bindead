//keywords smc 
#include "../include/reach.h"

//@summarystart
// A self-modifying code example\ 
//@summaryend
int main(void)
{
  /* To run this program on a real machine, the following code is 
   * needed to change the access mode of the given page 
   * (where the instruction 'cnt: mov ax, 0' belongs to).
   * 
   *   DWORD dwOldProtect = 0;
   *   VirtualProtect((LPVOID)page_starting_address,page_size,PAGE_WRITECOPY,&dwOldProtect);
   *
   *   e.g., page_starting_address: 0x4112da
   *         page_size: 4096
   */

  unsigned short Num;
  int f=0;
  if(f!=0) 
    goto __CHECK; /* This is only to make the IDApro                          *
                   * recognize the code after the _asm as "code" and not data */
  _asm {
    mov cx, 1;                 /* Store 1 into cx*/

    inc byte ptr [cnt + 1];    /* Modifies the instruction "mov ax,0" */
                               /* to "mov cx, 0" at label cnt */

    jmp cnt;                   /* Adding a "jmp cnt" between the   */
                               /* INC and the MOV will cause the   */
                               /* modified code to be run      */ 

 cnt: mov ax, 0;               /* This instruction is modified to "mov cx, 0" */

    mov Num, cx;               /* cx always has the value 0 here */
  }
 __CHECK:
#if REACH == 0
  if(Num == 1) { /* Num is always 0 */
    UNREACHABLE();
  }
#else
  if(Num == 0) { /* Num is always 0 */
    REACHABLE();
  }
#endif
  return 0;
}
