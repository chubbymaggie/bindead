// @summarystart
// Here, the developer is getting a pathname as an argument and wants
// to find the first path component. The error is that the path
// in str might start with a '/', in which case len is zero and
// len-1 is the largest value possible for a size_t. In that particular
// case the strncpy in the else clause is no safer than a strcpy.
// @summaryend

#include "../include/reach.h"

#define MAX_SIZE    32

char * csurf_replace_strchr(const char *s, int c)
{
  int n = 0;
  do {
    if (*s == c)
      {
	return (char*)s;
      }
  } while (*s++);
  return (0);
}
char * csurf_replace_strncpy(char * s1, char * s2, int n)
{
  int i;
  char * os1;

  os1 = s1;
  for (i = 0; i < n; i++)
    *s1++ = *s2++;
  return(os1);
}

int main() {
  char str[MAX_SIZE];
  char buf[MAX_SIZE];

  unsigned int len;

  char *firstslash;
 
  str[MAX_SIZE-1] = 0;

  firstslash = csurf_replace_strchr(str, '/');

  if (!firstslash) {
    csurf_replace_strncpy(buf, str, MAX_SIZE - 1);  /* leave room for the zero */
    buf[MAX_SIZE - 1] = 0;
  }
  else
  {
    len = firstslash - str;     /* length of the first path component */

    if (len > MAX_SIZE - 1)
      len = MAX_SIZE - 1;

    if(len - 1 > MAX_SIZE)
      REACHABLE();

    csurf_replace_strncpy(buf, str, len-1);   /* cut the slash off. Only copy len-1
				   characters to avoid zero padding.  */

    buf[len] = 0;
  }
  return 0;
}
