
#include <stdlib.h>

int f1 (long n) __attribute__ ((noinline));
int f1 (long n) {
  if (n > -100 && n < 100)
    return n + 98;
  return -1;
}

long f2 (int n) __attribute__ ((noinline));
long f2 (int n) {
  if (n > -100 && n < 100)
    return n + 98;
  return -1;
}

unsigned int f3 (int n) __attribute__ ((noinline));
unsigned int f3 (int n) {
  if (n > -100 && n < 100)
    return n + 99;
  return 0;
}

int main (void) {
  //int n1 = f1(random());
  unsigned long n2 = f3(rand());
  n2 -= 3;
  //n1 += f1((long)rand());
  //n1 += (int)f2((int)random());
  //n1 += (int)f2(rand());
  return n2;
}

